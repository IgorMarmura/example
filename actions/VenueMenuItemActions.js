import { venue_menu_item as venueMenuItemActionTypes } from '../constants/actionTypes';
import { venueMenuService }                            from '@services/VenueMenuService';

export const venueMenuItemActions = {
  getVenueMenuItemsTop,
  getVenueMenuItemsByVenueId,
  getVenueMenuItemsBySectionId,
  saveVenueMenuItem,
  updateVenueMenuItem,
  updateVenueMenuItemTop,
};

function getVenueMenuItemsTop() {
  return dispatch => {
    dispatch(request());

    venueMenuService.getVenueMenuItemsTop().then(
      res => {
        dispatch(success(res));
      },
      error => {
        dispatch(failure(error));
      }
    );
  };

  function request() { return {type: venueMenuItemActionTypes.VENUE_MENU_ITEM_TOP_LIST_REQUEST} }

  function success(data) {
    return {
      type                 : venueMenuItemActionTypes.VENUE_MENU_ITEM_TOP_LIST_SUCCESS,
      venueMenuItemsTopList: data
    }
  }

  function failure(error) {
    return {
      type: venueMenuItemActionTypes.VENUE_MENU_ITEM_TOP_LIST_FAILURE,
      error
    }
  }
}

function getVenueMenuItemsByVenueId() {
  return dispatch => {
    dispatch(request());

    venueMenuService.getVenueMenuItemsByVenueId().then(
      res => {
        dispatch(success(res));
      },
      error => {
        dispatch(failure(error));
      }
    );
  };

  function request() { return {type: venueMenuItemActionTypes.VENUE_MENU_ITEM_LIST_REQUEST} }

  function success(data) {
    return {
      type                 : venueMenuItemActionTypes.VENUE_MENU_ITEM_LIST_SUCCESS,
      venueMenuItemsList: data
    }
  }

  function failure(error) {
    return {
      type: venueMenuItemActionTypes.VENUE_MENU_ITEM_LIST_FAILURE,
      error
    }
  }
}

function getVenueMenuItemsBySectionId(sectionId) {
  return dispatch => {
    dispatch(request());

    venueMenuService.getVenueMenuItemsBySectionId(sectionId).then(
      res => {
        dispatch(success(res));
      },
      error => {
        dispatch(failure(error));
      }
    );
  };

  function request() { return {type: venueMenuItemActionTypes.VENUE_MENU_ITEM_LIST_REQUEST} }

  function success(data) {
    return {
      type                 : venueMenuItemActionTypes.VENUE_MENU_ITEM_LIST_SUCCESS,
      venueMenuItemsList: data
    }
  }

  function failure(error) {
    return {
      type: venueMenuItemActionTypes.VENUE_MENU_ITEM_LIST_FAILURE,
      error
    }
  }
}

function saveVenueMenuItem(data) {
  return dispatch => {
    dispatch(request());

    venueMenuService.saveMenuItem(data).then(
      res => {
        dispatch(success(res));
      },
      error => {
        dispatch(failure(error));
      }
    );
  };

  function request() { return {type: venueMenuItemActionTypes.VENUE_MENU_ITEM_ADD_REQUEST} }

  function success(res) {
    return {
      type         : venueMenuItemActionTypes.VENUE_MENU_ITEM_ADD_SUCCESS,
      savedMenuItem: {
        ...data,
        ...res
      }
    }
  }

  function failure(error) {
    return {
      type: venueMenuItemActionTypes.VENUE_MENU_ITEM_ADD_FAILURE,
      error
    }
  }
}

function updateVenueMenuItem(data) {
  return dispatch => {
    dispatch(request());

    venueMenuService.updateMenuItem(data).then(
      res => {
        dispatch(success(res));
      },
      error => {
        dispatch(failure(error));
      }
    );
  };

  function request() { return {type: venueMenuItemActionTypes.VENUE_MENU_ITEM_UPDATE_REQUEST} }

  function success() {
    return {
      type           : venueMenuItemActionTypes.VENUE_MENU_ITEM_UPDATE_SUCCESS,
      // updatedMenuItem: {id: id}
    }
  }

  function failure(error) {
    return {
      type: venueMenuItemActionTypes.VENUE_MENU_ITEM_UPDATE_FAILURE,
      error
    }
  }
}

function updateVenueMenuItemTop(data) {
  return dispatch => {
    dispatch(request());

    venueMenuService.updateMenuItemTop(data).then(
      res => {
        dispatch(success(res));
      },
      error => {
        dispatch(failure(error));
      }
    );
  };

  function request() { return {type: venueMenuItemActionTypes.VENUE_MENU_ITEM_TOP_LIST_UPDATE_REQUEST} }

  function success() {
    return {
      type           : venueMenuItemActionTypes.VENUE_MENU_ITEM_TOP_LIST_UPDATE_SUCCESS,
      // updatedMenuItem: {id: id}
    }
  }

  function failure(error) {
    return {
      type: venueMenuItemActionTypes.VENUE_MENU_ITEM_TOP_LIST_UPDATE_FAILURE,
      error
    }
  }
}