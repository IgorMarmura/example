import { venue_menu_item as venueMenuItemActionTypes } from '../constants/actionTypes';

export default function venueMenuItem(state = {}, action) {
  switch (action.type) {

    case venueMenuItemActionTypes.VENUE_MENU_ITEM_TOP_LIST_REQUEST:
      return {
        loading: true
      };

    case venueMenuItemActionTypes.VENUE_MENU_ITEM_TOP_LIST_SUCCESS:
      return {
        ...state,
        topList: action.venueMenuItemsTopList.slice(0),
        loading: false
      };

    case venueMenuItemActionTypes.VENUE_MENU_ITEM_TOP_LIST_FAILURE:
      return {
        error: action.error
      };

    case venueMenuItemActionTypes.VENUE_MENU_ITEM_LIST_REQUEST:
      return {
        loading: true
      };

    case venueMenuItemActionTypes.VENUE_MENU_ITEM_LIST_SUCCESS:
      return {list: action.venueMenuItemsList.slice(0)};

    case venueMenuItemActionTypes.VENUE_MENU_ITEM_LIST_FAILURE:
      return {
        error: action.error
      };

    case venueMenuItemActionTypes.VENUE_MENU_ITEM_ADD_REQUEST:
      return {
        ...state,
        loading: true
      };

    case venueMenuItemActionTypes.VENUE_MENU_ITEM_ADD_SUCCESS:
      let list = state.list.slice(0);
      list.push(action.savedMenuItem);
      return {
        ...state,
        list   : list,
        loading: false
      };

    case venueMenuItemActionTypes.VENUE_MENU_ITEM_ADD_FAILURE:
      return {
        error: action.error
      };

    case venueMenuItemActionTypes.VENUE_MENU_ITEM_UPDATE_REQUEST:
      return {
        ...state,
        loading: true
      };

    case venueMenuItemActionTypes.VENUE_MENU_ITEM_UPDATE_SUCCESS:
      return {
        ...state,
        loading: false
      };

    case venueMenuItemActionTypes.VENUE_MENU_ITEM_UPDATE_FAILURE:
      return {
        error: action.error
      };


    case venueMenuItemActionTypes.VENUE_MENU_ITEM_TOP_LIST_UPDATE_REQUEST:
      return {
        ...state,
        loading: true
      };

    case venueMenuItemActionTypes.VENUE_MENU_ITEM_TOP_LIST_UPDATE_SUCCESS:
      return {
        ...state,
        loading: false
      };

    case venueMenuItemActionTypes.VENUE_MENU_ITEM_TOP_LIST_UPDATE_FAILURE:
      return {
        error: action.error
      };

    case venueMenuItemActionTypes.VENUE_MENU_ITEM_DELETE_REQUEST:
      return {
        ...state,
        loading: true
      };

    case venueMenuItemActionTypes.VENUE_MENU_ITEM_DELETE_SUCCESS:
      let filteredList = state.list.slice(0).filter((item) => item.id !== action.venueDeletedMenuSection.id);
      return {
        ...state,
        list   : filteredList,
        loading: false
      };

    case venueMenuItemActionTypes.VENUE_MENU_ITEM_DELETE_FAILURE:
      return {
        error: action.error
      };

    default:
      return state
  }
}