import React, { Component }     from 'react';
import './menu.css';
import MenuItem                 from './MenuItem';
import MenuItemAdd              from './MenuItemAdd';
import { venueMenuItemActions } from '@actions/VenueMenuItemActions';
import { bindActionCreators }   from 'redux';
import { connect }              from 'react-redux';

const mapStateToProps = (state) => ({
  items: state.venueMenuItem.list
});
const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(
    {...venueMenuItemActions},
    dispatch
  )
});

class MenuItemsScreen extends Component {

  componentDidMount() {
    this.props.actions.getVenueMenuItemsBySectionId(this.props.match.params.id);
  }

  createItem = (data) => {
    data.venue_menu_section_id = parseInt(this.props.match.params.id);
    this.props.actions.saveVenueMenuItem(data);
  };

  updateItem = (data) => {
    this.props.actions.updateVenueMenuItem(data);
  };

  render() {
    return (
      <div className='venue-menu-content'>
        <h4 className='content-title'>Venue Items</h4>
        <div className="table-responsive table-hover table-bordered table-centered">
          <table className="table">
            <thead className='thead-dark'>
            <tr>
              <th className='text-center'>#</th>
              <th>Meta</th>
              <th>Size</th>
              <th>Price</th>
              <th className='text-center'>Is Active</th>
              <th className='text-center'>Actions</th>
            </tr>
            </thead>
            <tbody>
            {this.props.items.map((section, index) => {
              return <MenuItem key={index}
                               updateItem={this.updateItem}
                               section={section}/>;
            })}
            <MenuItemAdd createItem={this.createItem}/>
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}

MenuItemsScreen.defaultProps = {
  items: []
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MenuItemsScreen);