import React, { Component } from 'react';
import { Link }             from 'react-router-dom';

class MenuSection extends Component {

  state = {
    name     : '',
    is_active: false,
    changed  : false
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (!prevState.id && nextProps.section) {
      return {
        ...prevState,
        ...nextProps.section
      };
    }

    return prevState;
  }

  constructor(props) {
    super(props);
    this.nameInput = React.createRef();
  }

  handleInputChange = () => {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name] : value,
      changed: true
    });
  };

  handleCheckboxChange = () => {
    this.setState({
      is_active: !this.state.is_active,
      changed  : true
    });
  };

  cancel = () => {
    this.setState({
      changed  : false,
      is_active: this.props.section.is_active
    });
    this.nameInput.current.value = this.props.section.name;
  };

  save = () => {
    this.props.updateSection(this.state);
    this.setState({changed: false})
  };

  render() {
    const {
      name,
      is_active,
      changed
    } = this.state;

    return (
      <tr>
        <td className='text-center'>{this.props.section.order_index}</td>
        <td>
          <input type="text"
                 className='form-control'
                 placeholder='Name'
                 name='name'
                 onChange={this.handleInputChange}
                 ref={this.nameInput}
                 defaultValue={name}/>
        </td>
        <td className='text-center'>
          <input type="checkbox"
                 checked={is_active}
                 onChange={() => true}
                 data-switch="success"/>
          <label data-on-label="Yes"
                 data-off-label="No"
                 onClick={this.handleCheckboxChange}
                 className="mb-0 d-block"/>
        </td>
        <td className='text-center'>

          {changed &&
          <button className='btn-sm btn-primary mr-3'
                  onClick={this.save}
                  type='button'>Save</button>
          }
          {changed &&
          <button className='btn-sm btn-light'
                  onClick={this.cancel}
                  type='button'>Cancel</button>
          }

          {!changed &&
          <button className='btn-sm btn-light mr-3'
                  onClick={this.cancel}
                  type='button'>
            <Link to={'menu/section/' + this.props.section.id}>View items</Link>
          </button>

          }

          {!changed &&
          <button className='btn-sm btn-danger'
                  type='button'>Delete</button>
          }
        </td>
      </tr>
    )
  }
}

export default MenuSection;