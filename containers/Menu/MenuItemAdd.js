import React, { Component } from 'react';

class MenuItemAdd extends Component {

  state = {
    name       : '',
    description: '',
    size       : '',
    price      : '',
    is_active: false
  };

  constructor(props) {
    super(props);
    this.nameInput = React.createRef();
    this.descriptionInput = React.createRef();
    this.sizeInput = React.createRef();
    this.priceInput = React.createRef();
  }

  _handleClick = () => {
    this.props.createItem(this.state);
    this.nameInput.current.value = '';
    this.descriptionInput.current.value = '';
    this.sizeInput.current.value = '';
    this.priceInput.current.value = '';
  };

  _handleInputChange = () => {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  };

  _handleCheckboxChange = () => {
    this.setState({
      is_active: !this.state.is_active
    });
  };

  render() {
    return (
      <tr>
        <td className='text-center'>#</td>
        <td>
          <input type="text"
                 onChange={this._handleInputChange}
                 name='name'
                 className='form-control'
                 ref={this.nameInput}
                 placeholder='Item name'/>
          <textarea name="description"
                    cols="30"
                    rows="2"
                    onChange={this._handleInputChange}
                    ref={this.descriptionInput}
                    className='form-control mt-5'
                    placeholder='Description' />
        </td>
        <td className='text-center'>
          <input type="text"
                 name='size'
                 onChange={this._handleInputChange}
                 ref={this.sizeInput}
                 className='form-control'
                 placeholder='Size'/>
        </td>
        <td className='text-center'>
          <input type="text"
                 name='price'
                 onChange={this._handleInputChange}
                 ref={this.priceInput}
                 className='form-control'
                 placeholder='Price'/>
        </td>
        <td className='text-center'>
          <input type="checkbox"
                 checked={this.state.is_active}
                 onChange={() => true}
                 data-switch="success"/>
          <label data-on-label="Yes"
                 data-off-label="No"
                 onClick={this._handleCheckboxChange}
                 className="mb-0 d-block"/>

        </td>
        <td className='text-center'>
          <button className='btn-sm btn-primary'
                  onClick={this._handleClick}
                  type='button'>Create
          </button>
        </td>
      </tr>
    )
  }
}

export default MenuItemAdd;