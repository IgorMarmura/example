import React, { Component }        from 'react';
import './menu.css';
import MenuSection                 from './MenuSection';
import MenuSectionAdd              from './MenuSectionAdd';
import { venueMenuSectionActions } from '@actions/VenueMenuSectionActions';
import { venueMenuItemActions }    from '@actions/VenueMenuItemActions';
import { bindActionCreators }      from 'redux';
import { connect }                 from 'react-redux';
import MenuTopItemsSelect          from './MenuTopItemsSelect';

const mapStateToProps = (state) => ({
  sections: state.venueMenuSection.list,
  items   : state.venueMenuItem.list,
  topItems: state.venueMenuItem.topList,
});
const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(
    {...venueMenuSectionActions, ...venueMenuItemActions},
    dispatch
  )
});

class MenuScreen extends Component {

  componentDidMount() {
    this.props.actions.getVenueMenuSections();
    this.props.actions.getVenueMenuItemsByVenueId();
    this.props.actions.getVenueMenuItemsTop();
  }

  createSection = (data) => {
    this.props.actions.saveVenueMenuSection(data);
  };

  updateSection = (data) => {
    this.props.actions.updateVenueMenuSection(data);
  };

  updateItemsTop = (data) => {
    this.props.actions.updateVenueMenuItemTop(data);
  };

  render() {
    return (
      <div className='venue-menu-content'>
        <h4 className='content-title'>TOP 5 items</h4>
        <MenuTopItemsSelect topItems={this.props.topItems}
                            items={this.props.items}
                            updateItemsTop={this.updateItemsTop}/>

        <h4 className='content-title'>Menu Sections</h4>
        <div className="table-responsive table-hover table-bordered table-centered">
          <table className="table">
            <thead className='thead-dark'>
            <tr>
              <th className='text-center'>#</th>
              <th>Name</th>
              <th className='text-center'>Is Active</th>
              <th className='text-center'>Actions</th>
            </tr>
            </thead>
            <tbody>
            {this.props.sections.map((section, index) => {
              return <MenuSection key={index}
                                  updateSection={this.updateSection}
                                  section={section}/>;
            })}
            <MenuSectionAdd createSection={this.createSection}/>
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}

MenuScreen.defaultProps = {
  sections: []
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MenuScreen);