import React, { Component } from 'react';

class MenuSectionAdd extends Component {

  state = {
    name: '',
    is_active: false
  };

  constructor(props) {
    super(props);
    this.nameInput = React.createRef();
  }

  handleClick = () => {
    this.props.createSection(this.state);
    this.nameInput.current.value = '';
  };

  handleInputChange = () => {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  };

  handleCheckboxChange = () => {
    this.setState({
      is_active: !this.state.is_active
    });
  };

  render() {
    return (
      <tr>
        <td className='text-center'>#</td>
        <td>
          <input type="text"
                 onChange={this.handleInputChange}
                 name='name'
                 className='form-control'
                 ref={this.nameInput}
                 placeholder='Section name'/>
        </td>
        <td className='text-center'>
          <input type="checkbox"
                 checked={this.state.is_active}
                 onChange={() => true}
                 data-switch="success"/>
          <label data-on-label="Yes"
                 data-off-label="No"
                 onClick={this.handleCheckboxChange}
                 className="mb-0 d-block"/>

        </td>
        <td className='text-center'>
          <button className='btn-sm btn-primary'
                  onClick={this.handleClick}
                  type='button'>Create
          </button>
        </td>
      </tr>
    )
  }
}

export default MenuSectionAdd;