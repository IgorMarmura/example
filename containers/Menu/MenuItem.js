import React, { Component } from 'react';

class MenuItem extends Component {

  state = {
    name       : '',
    description: '',
    size       : '',
    price      : '',
    is_active  : false,
    changed    : false
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (!prevState.id && nextProps.section) {
      return {
        ...prevState,
        ...nextProps.section
      };
    }

    return prevState;
  }

  constructor(props) {
    super(props);
    this.nameInput = React.createRef();
  }

  _handleInputChange = () => {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name] : value,
      changed: true
    });
  };

  _handleCheckboxChange = () => {
    this.setState({
      is_active: !this.state.is_active,
      changed  : true
    });
  };

  _cancel = () => {
    this.setState({
      changed  : false,
      is_active: this.props.section.is_active
    });
    this.nameInput.current.value = this.props.section.name;
    this.nameInput.current.description = this.props.section.description;
    this.nameInput.current.size = this.props.section.size;
    this.nameInput.current.price = this.props.section.price;
  };

  _save = () => {
    this.props.updateItem(this.state);
    this.setState({changed: false})
  };

  render() {
    const {
      name,
      description,
      size,
      price,
      is_active,
      changed
    } = this.state;

    return (
      <tr>
        <td className='text-center'>{this.props.section.order_index}</td>
        <td>
          <input type="text"
                 className='form-control'
                 placeholder='Name'
                 name='name'
                 onChange={this._handleInputChange}
                 ref={this.nameInput}
                 defaultValue={name}/>
          <textarea name="description"
                    cols="30"
                    rows="2"
                    onChange={this._handleInputChange}
                    className='form-control mt-5'
                    placeholder='Description'
                    defaultValue={description}/>
        </td>
        <td className='text-center'>
          <input type="text"
                 className='form-control'
                 placeholder='Size'
                 onChange={this._handleInputChange}
                 defaultValue={size}/>
        </td>
        <td className='text-center'>
          <input type="text"
                 className='form-control'
                 placeholder='Price'
                 onChange={this._handleInputChange}
                 defaultValue={price}/>
        </td>
        <td className='text-center'>
          <input type="checkbox"
                 checked={is_active}
                 onChange={() => true}
                 data-switch="success"/>
          <label data-on-label="Yes"
                 data-off-label="No"
                 onClick={this._handleCheckboxChange}
                 className="mb-0 d-block"/>
        </td>
        <td className='text-center'>

          {changed &&
          <button className='btn-sm btn-primary mr-3'
                  onClick={this._save}
                  type='button'>Save</button>
          }
          {changed &&
          <button className='btn-sm btn-light'
                  onClick={this._cancel}
                  type='button'>Cancel</button>
          }

          {!changed &&
          <button className='btn-sm btn-danger'
                  type='button'>Delete</button>
          }
        </td>
      </tr>
    )
  }
}

export default MenuItem;