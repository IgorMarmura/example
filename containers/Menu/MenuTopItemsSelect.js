import React, { Component } from 'react';
import Select               from 'react-select';
import {
  menu
} from '@utils';

class MenuTopItemsSelect extends Component {

  state = {
    selectedItems: []
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (!prevState.selectedItems.length && nextProps.topItems) {
      return {
        ...prevState,
        selectedItems: menu.prepareMenuItemsListForSelect(nextProps.topItems)
      };
    }

    return prevState;
  }

  handleSelectChange = (selectedOption) => {
    this.setState({
      selectedItems: selectedOption
    });
    this.props.updateItemsTop(selectedOption);
  };

  render() {
    const items = menu.prepareMenuItemsListForSelect(this.props.items);

    return(
      <div className='mb-25'>
        <Select
          value={this.state.selectedItems}
          onChange={this.handleSelectChange}
          options={items}
          isMulti={true}
        />
      </div>
    )
  }
}

MenuTopItemsSelect.defaultProps = {
  items: []
};

export default MenuTopItemsSelect;