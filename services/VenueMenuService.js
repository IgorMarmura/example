import { server } from '../constants/server';
import {
  auth,
  response
}                 from '@utils';

export const venueMenuService = {
  getMenuSections,
  saveMenuSection,
  updateMenuSection,
  getVenueMenuItemsTop,
  getVenueMenuItemsByVenueId,
  getVenueMenuItemsBySectionId,
  saveMenuItem,
  updateMenuItem,
  updateMenuItemTop,
};

function getMenuSections() {
  const requestOptions = {
    method : 'GET',
    headers: new Headers(auth.authHeader()),
  };

  return fetch(`${server.API_URL}/venue/menu/section/list/` + auth.user().venue_id, requestOptions)
    .then(response.handleResponse)
    .then(res => {
      return res;
    });
}

function saveMenuSection(data) {
  data.venue_id = auth.user().venue_id;

  const requestOptions = {
    method : 'POST',
    headers: new Headers(auth.authHeader()),
    body   : JSON.stringify(data)
  };

  return fetch(`${server.API_URL}/venue/menu/section/create`, requestOptions)
    .then(response.handleResponse)
    .then(res => {
      return res;
    });
}

function updateMenuSection(data) {
  const requestOptions = {
    method : 'POST',
    headers: new Headers(auth.authHeader()),
    body   : JSON.stringify(data)
  };

  return fetch(`${server.API_URL}/venue/menu/section/update`, requestOptions)
    .then(response.handleResponse)
    .then(res => {
      return res;
    });
}

function getVenueMenuItemsTop() {
  const requestOptions = {
    method : 'GET',
    headers: new Headers(auth.authHeader()),
  };

  return fetch(`${server.API_URL}/venue/menu/item/list/top/` + auth.user().venue_id, requestOptions)
    .then(response.handleResponse)
    .then(res => {
      return res;
    });
}

function getVenueMenuItemsByVenueId() {
  const requestOptions = {
    method : 'GET',
    headers: new Headers(auth.authHeader()),
  };

  return fetch(`${server.API_URL}/venue/menu/item/list/` + auth.user().venue_id, requestOptions)
    .then(response.handleResponse)
    .then(res => {
      return res;
    });
}

function getVenueMenuItemsBySectionId(sectionId) {
  const requestOptions = {
    method : 'GET',
    headers: new Headers(auth.authHeader()),
  };

  return fetch(`${server.API_URL}/venue/menu/item/list/section/` + sectionId, requestOptions)
    .then(response.handleResponse)
    .then(res => {
      return res;
    });
}

function saveMenuItem(data) {
  data.venue_id = auth.user().venue_id;

  const requestOptions = {
    method : 'POST',
    headers: new Headers(auth.authHeader()),
    body   : JSON.stringify(data)
  };

  return fetch(`${server.API_URL}/venue/menu/item/create`, requestOptions)
    .then(response.handleResponse)
    .then(res => {
      return res;
    });
}

function updateMenuItem(data) {

  const requestOptions = {
    method : 'POST',
    headers: new Headers(auth.authHeader()),
    body   : JSON.stringify(data)
  };

  return fetch(`${server.API_URL}/venue/menu/item/update`, requestOptions)
    .then(response.handleResponse)
    .then(res => {
      return res;
    });
}

function updateMenuItemTop(data) {
  const requestOptions = {
    method : 'POST',
    headers: new Headers(auth.authHeader()),
    body   : JSON.stringify({ items: data })
  };

  return fetch(`${server.API_URL}/venue/menu/item/list/top/update`, requestOptions)
    .then(response.handleResponse)
    .then(res => {
      return res;
    });
}